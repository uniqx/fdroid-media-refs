# SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
#
# SPDX-License-Identifier: CC-BY-SA-4.0
# SPDX-License-Identifier: GPL-3.0-or-later

---
web:

  de:

    - title: Topio e.V. – Digitale Selbstbestimmung in der Arminiusmarkthalle
      date: 2021-12-07
      url: https://moabitonline.de/37290

    - title: F-Droid als App-Bezugsquelle
      date: 2021-12-02
      url: https://anoxinon.media/blog/f-droid_als_app_bezugsquelle/

    - title: Open-Source-Adventskalender: Die Play-Store-Alternative F-Droid
      date: 2021-12-02
      author: Stefan Mey
      url: https://www.heise.de/news/Open-Source-Adventskalender-Die-Play-Store-Alternative-F-Droid-6281316.html

    - title: F-droid markiert Apps mit Tracker
      date: 2021-04-24
      author: Lars Müller
      url: https://dasnetzundich.de/2021/04/24/f-droid-markiert-apps-mit-tracker/

    - title: Smartphone-Schnüfflern den Riegel vorschieben
      date: 2020-01-14
      author: dpa
      url: https://www.zeit.de/news/2020-01/14/smartphone-schnuefflern-den-riegel-vorschieben

    - title: Privatsphäre im Netz - Tschüss, Google!
      date: 2020-01-06
      author: Ulf Schleth
      url: https://taz.de/Privatsphaere-im-Netz/!5650071/

    - title: App-Berechtigungen bei F-Droid
      date: 2019-07-10
      author: Inga Pöting
      url: https://mobilsicher.de/ratgeber/berechtigungen-bei-f-droid

    - title: Marktplatz F-Droid - Zehn freundliche Android-Apps
      date: 2018-12-22
      author:
        - Stefan Mey
      url: https://www.zdf.de/nachrichten/heute/zehn-freundliche-android-apps-100.html

    - title: Öffi statt bei Google nun bei F-Droid – und bald Amazon
      date: 2018-08-27
      url: https://www.derstandard.at/story/2000086125972/oeffi-statt-bei-google-nun-bei-f-droid-und-bald

    - title: "Nach Play-Store-Bann: Öffi wird Open Source"
      date: 2018-07-18
      url: https://www.derstandard.at/story/2000083688590/nach-play-store-bann-oeffi-wird-open-source

    - title: So installieren Sie den App-Store F-Droid
      date: 2018-02-08
      updated: 2019-07-10
      author: Inga Pöting
      url: https://mobilsicher.de/ratgeber/so-installieren-sie-den-app-store-f-droid

    - title: "F-Droid: Verbraucherfreundlicher App-Store für Android"
      date: 2016-01-29
      updated: 2019-12-05
      author: Miriam Ruhenstroth
      url: https://mobilsicher.de/ratgeber/verbraucherfreundlich-f-droid

    - title: Android, bitte freimachen
      date: 2012-03-02
      author: Eike Kühl
      url: https://www.zeit.de/digital/mobil/2012-03/android-freie-software-cyanogenmod

  en:

    - title: Best F-Droid apps to replace your existing apps
      date: 2020-03-03
      author: AJAAY
      url: https://www.theandroidsoul.com/best-f-droid-apps/

    - title: How to sync Nextcloud calendars with Android
      date: 2020-02-26
      author: Jack Wallen
      url: https://www.techrepublic.com/article/how-to-sync-nextcloud-calendars-with-android/

    - title: Top 5 Alternatives to F-Droid – Download More Apps
      date: 2020-02-18
      author: Amy Wagstaff
      url: https://henrilechatnoir.com/top-5-alternatives-to-f-droid-download-more-apps/6694/

    - title: Top 5 Best Apps on F-Driod Store (Not on Google Play Store)
      date: 2019-12-31
      author: Nikhil Azza
      url: https://www.digitbin.com/best-apps-f-driod/

    - title: Jitsi Meet now available on F-Droid
      date: 2019-11-26
      url: https://jitsi.org/news/jitsi-meet-now-available-on-f-droid/

    - title: |-
          F-Droid: A security-conscious repository for Free and
          Open Source Software (FOSS) applications for Android
      date: 2019-08-30
      author: Kathleen Chapman
      url: https://the-gadgeteer.com/2019/08/30/f-droid-a-security-conscious-repository-for-free-and-open-source-software-foss-applications-for-android/

    - title: Fight Android malware by quitting Google Play and using F-Droid for Android apps
      date: 2019-08-06
      author: Rae Hodge
      url: https://www.cnet.com/how-to/fight-android-malware-by-quitting-google-play-and-using-f-droid-to-install-android-apps/

    - title: Going Gapp-Less, Again….
      date: 2019-07-19
      url: https://thealaskalinuxuser.wordpress.com/2019/07/11/going-gapp-less-again/

    - title: Kali NetHunter App Store Now in Public Beta
      date: 2019-07-17
      author: Dark Reading
      url: https://web.archive.org/web/20190717175156/https://www.darkreading.com/application-security/kali-nethunter-app-store-now-in-public-beta/d/d-id/1335274

    - title: How does Android feel without Google?
      date: 2019-05-26
      author: Eric Ferrari-Herrmann
      url: https://www.androidpit.com/android-without-google-apps

    - title: How to Use F-Droid & The Best Apps for F-Droid
      date: 2019-04-05
      author: Douglas Crawford
      url: https://proprivacy.com/privacy-service/guides/f-droid

    - title: What is F-Droid and how to install it on Android
      date: 2018-12-19
      author: Usama M
      url: https://techbeasts.com/what-is-f-droid-and-how-to-install-it-on-android/

    - title: 10 Exclusive F-Droid Apps You Can’t Get on Google Play Store
      date: 2018-10-25
      author: Dan Price
      url: https://www.makeuseof.com/tag/exclusive-f-droid-apps/

    - title: 5 Exclusive F-Droid Android Apps You Should Try
      date: 2018-10-27
      author: Vivek
      url: https://www.droidviews.com/5-exclusive-f-droid-android-apps/

    - title: "FOSS Project Spotlight: Tutanota, the First Encrypted Email Service with an App on F-Droid"
      date: 2018-10-11
      author: Matthias Pfau
      url: https://www.linuxjournal.com/content/foss-project-spotlight-tutanota-first-encrypted-email-service-app-f-droid

    - title: This is what it's like using only open-source software on Android
      date: 2018-04-29
      author: Corbin Davenport
      url: https://www.androidpolice.com/2018/04/29/like-using-open-source-software-android/

    - title: "Quality Open Source Apps: Best of F-Droid (2018)"
      date: 2018-03-30
      author: Konrad Iturbe
      url: https://medium.com/@konrad_it/quality-open-source-apps-best-of-f-droid-2018-fca018e59891

    - title: Fairphone ships phones with F-Droid integrated; auto-update enabled
      date: 2018-01-27
      url: https://ibcomputing.com/fairphone-f-droid-auto-update/

    - title: "F-Droid: A free, open, privacy-oriented Android app store that corrects Android's \"original sin\""
      date: 2018-01-22
      author: Cory Doctorow
      url: https://boingboing.net/2018/01/22/f-droid.html

    - title: "Android Users: To Avoid Malware, Try the F-Droid App Store"
      date: 2018-01-21
      author:
        - Sean O'Brien
        - Michael Kwet
      url: https://www.wired.com/story/android-users-to-avoid-malware-ditch-googles-app-store/

    - title: F-Droid 1.0 for Android is out
      date: 2017-10-20
      author: Martin Brinkmann
      url: https://www.ghacks.net/2017/10/20/f-droid-1-0-for-android-is-out/

    - title: F-Droid, the open-source app repository, has been updated to v1.0
      date: 2017-10-19
      author: Ryne Hager
      url: https://www.androidpolice.com/2017/10/19/f-droid-open-source-app-repository-updated-v1-0/

    - title: How to get Android apps without using the Play Store
      date: 2017-08-04
      author: Brad Linder
      url: https://liliputing.com/2017/08/get-android-apps-without-using-play-store.html

    - title: Top 10 F-Droid Best Apps that are Not on Google Play
      date: 2017-08-02
      author: Rishabh Maheshwari
      url: https://techwiser.com/f-droid-best-apps/

    - title: F-Droid – A Collection of Free Android App Repository
      date: 2017-07-20
      author: John Paul Wohlscheid
      url: https://www.fossmint.com/f-droid-a-collection-of-free-android-app-repository/

    - title: F-Droid's lead app maintainer Boris Kraut is leaving the project
      date: 2017-07-18
      author: Ryne Hager
      url: https://www.androidpolice.com/2017/06/18/f-droid-lead-app-maintainer-boris-kraut-leaving-project/

    - title: F-Droid finally sees user interface improvements after six years
      date: 2017-05-02
      author: John Hoff
      url: https://androidcommunity.com/f-droid-finally-sees-user-interface-improvements-after-six-years-20170502/

    - title: F-Droid’s Android App Finally Gets a UI Makeover
      date: 2017-04-28
      author: Aamir Siddiqui
      url: https://www.xda-developers.com/f-droids-android-app-finally-gets-a-ux-makeover/

    - title: F-Droid gets a makeover (app store for free and open source Android apps)
      date: 2017-04-28
      author: Brad Linder
      url: https://liliputing.com/2017/04/f-droid-gets-makeover-app-store-free-open-source-android-apps.html

    - title: "How to survive Android without Google: the FOSS version"
      date: 2016-05-03
      author: Andrew Orr
      url: https://www.androidguys.com/featured/opinion/how-to-survive-android-without-google-the-foss-version/

    - title: New F-Droid update released, APK ready for download
      date: 2015-11-20
      author: Rei Padla
      url: https://androidcommunity.com/new-f-droid-update-released-apk-ready-for-download-20151120/

    - title: "Pro tip: Find tons of open-source Android software with F-Droid"
      date: 2015-01-28
      author: Jack Wallen
      url: https://www.techrepublic.com/article/pro-tip-find-tons-of-open-source-android-software-with-f-droid/

    - title: Going open source on Android with F-Droid
      date: 2015-01-21
      author: Scott Nesbitt
      url: https://opensource.com/life/15/1/going-open-source-android-f-droid

    - title: "The great Ars experiment—free and open source software on a smartphone?!"
      date: 2014-07-29
      author: Ron Amadeo
      url: https://arstechnica.com/gadgets/2014/07/exploring-the-world-of-foss-android-can-a-smartphone-be-open-source/

    - title: "Smartphone security on a budget: four apps to try on Android and iPhone"
      date: 2014-05-13
      author: Jay McGregor
      url: https://www.theguardian.com/technology/2014/may/13/smartphone-security-obscuracam-cryptocat-mykolab

    - title: Interview with Ciaran Gultnieks of F-Droid
      date: 2014-05-08
      author: Joshua Gay
      url: https://www.fsf.org/blogs/licensing/interview-with-ciaran-gultnieks-of-f-droid

    - title: "Open-Source Android App Store: F-Droid"
      date: 2014-04-28
      author: HP
      url: https://papidroid.wordpress.com/2014/04/28/open-source-android-app-store-f-droid/

    - title: Use F-Droid to Install Open Source Android Apps
      date: 2013-07-29
      author: Trevor Dobrygoski
      url: https://www.maketecheasier.com/use-f-droid-to-install-open-source-android-apps/

    - title: "FSF fandroids fight to 'free' Android from Google's forepaws"
      date: 2012-03-06
      author: Gavin Clarke
      url: https://www.theregister.co.uk/2012/03/06/fsf_android_phone/

  pt:
    - title: "A loja de apps para Android de um mundo ideal existe"
      date: 2021-02-25
      author: Rodrigo Ghedin
      url: https://manualdousuario.net/f-droid-lojas-apps-android/
