<!--
SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>

SPDX-License-Identifier: CC0-1.0
-->

# fdroid-media-refs

Collection of refrences to all sorts of media reports, research, related to
F-Droid.

Since F-Droid does not surveil users there are no metrics on F-Droid useage.
This is an attempt to make public interest in F-Droid measureable by cataloging
all sorts of media about F-Droid. I'm interested in figuring out whether we
can use this data to improve F-Droid or at least help us understand how F-Droid
is percieved by the general public.

This dataset here is in all likelyhood far from complete and currently manually
currated. 
